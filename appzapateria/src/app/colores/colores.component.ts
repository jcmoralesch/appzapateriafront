import { Component, OnInit } from '@angular/core';
import { ColorService } from '../model/service/color.service';
import {Color} from '../model/class/color';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-colores',
  templateUrl: './colores.component.html',
  styleUrls: ['./colores.component.css']
})
export class ColoresComponent implements OnInit {
  
  public colors:Color[];
  public color:Color = new Color();

  constructor(public colorService:ColorService,
              private toastr:ToastrService) { }

  ngOnInit() {
    this.getAll();
  }

  private getAll():void{
    this.colorService.getAll().subscribe(
       colors=>this.colors=colors
    )
  }


  public store():void{
    this.colorService.store(this.color).subscribe(
      color=>{
        this.toastr.success("Operación realizada con éxito",'Success');
        this.getAll();
        this.cerrarModal();
        
      }
    )
  }

  public abrirModal(){
    this.colorService.abrirModal();
  }

  public cerrarModal():void{
    this.colorService.cerrarModal();
  }


}
