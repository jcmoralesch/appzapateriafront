import { Component, OnInit } from '@angular/core';
import { MarcaService } from '../model/service/marca.service';
import { ToastrService } from 'ngx-toastr';
import { Marca } from '../model/class/marca';

@Component({
  selector: 'app-marca',
  templateUrl: './marca.component.html',
  styleUrls: ['./marca.component.css']
})
export class MarcaComponent implements OnInit {

  public marcas:Marca[];
  public marca:Marca = new Marca();

  constructor(public marcaService:MarcaService,
              private toastr:ToastrService) { }

  ngOnInit() {
    this.getAll();
  }

  public store():void{

    this.marcaService.store(this.marca).subscribe(
      marca=>{
        this.getAll();
        this.cerrarModal();
        this.toastr.success('Operación realizada con éxito','Success');
      },
      err=>{
        
      }
    )

  }

  public getAll():void{
      this.marcaService.getAll().subscribe(
        marca=>this.marcas=marca
      )
  }

  public abrirModal():void{
    this.marcaService.abrirModal();
  }

  public cerrarModal():void{
    this.marcaService.cerrarModal();
  }

}
