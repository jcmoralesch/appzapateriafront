import { Component, OnInit } from '@angular/core';
import { LoginService } from '../model/service/login.service';

@Component({
  selector: 'app-nav-lateral',
  templateUrl: './nav-lateral.component.html',
  styleUrls: ['./nav-lateral.component.css']
})
export class NavLateralComponent implements OnInit {

  constructor(public loginService:LoginService) { }

  ngOnInit() {
  }

}
