import { Component, OnInit } from '@angular/core';
import { PersonalService } from 'src/app/model/service/personal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AgenciaService } from 'src/app/model/service/agencia.service';
import { Personal } from 'src/app/model/class/personal';
import Swal from 'sweetalert2';
import { Agencia } from 'src/app/model/class/agencia';

@Component({
  selector: 'app-form-personal',
  templateUrl: './form-personal.component.html',
  styleUrls: ['./form-personal.component.css']
})
export class FormPersonalComponent implements OnInit {

  public personal:Personal = new Personal();
  public agencias:Agencia[];

  constructor(private personalService:PersonalService,
              private router:Router,
              private agenciaService:AgenciaService,
              private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.getAllAgencias();
    this.cargarPersonal();
  }

  store():void{
    this.personalService.store(this.personal).subscribe(
      personal=>{
        this.router.navigate(['/personal']);
        Swal.fire('Registro nuevo',`${personal.nombre} registrado con éxito`,'success');
      }
    )
  }

  private getAllAgencias():void{
    this.agenciaService.getAll().subscribe(
      agencias=>this.agencias=agencias
    )
  }

  private cargarPersonal():void{
     this.activatedRoute.params.subscribe(
       param=>{
         let id=param['id'];
         if(id){
           this.personalService.getById(id).subscribe(
             personal=>this.personal=personal
           )
         }
       }
     )
  }

  public update():void{
    this.personalService.update(this.personal).subscribe(
      personal=>{
        this.router.navigate(['/personal']);
        Swal.fire('Actualizado','Datos actualizados correctamente','success');
      }
    )
  }

  

  compararAgencia(o1:Agencia,o2:Agencia):boolean{
    if(o1===undefined && o2===undefined){
      return true;
    }
    return o1===null || o2===null || o1===undefined || o2===undefined? false: o1.id===o2.id;
  }


}
