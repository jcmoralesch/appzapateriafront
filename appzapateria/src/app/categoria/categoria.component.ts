import { Component, OnInit } from '@angular/core';
import { CategoriaService } from '../model/service/categoria.service';
import { ThrowStmt } from '@angular/compiler';
import { Categoria } from '../model/class/categoria';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {

  public categorias:Categoria[];
  public categoria:Categoria= new Categoria();

  constructor(public categoriaService:CategoriaService,
             private toastr:ToastrService) { }
  
  ngOnInit() {
    this.getAll();
  }

  public store():void{
    this.categoriaService.store(this.categoria).subscribe(
      categoria=>{
        this.getAll();
        this.cerrarModal();
        this.toastr.success('Operación realizada con éxito','Success');
      },
      err=>{

      }
    )
  }

  private getAll():void{
    this.categoriaService.findAll().subscribe(
      categorias=>this.categorias=categorias
    )
  }

  public abrirModal(){
    this.categoriaService.abrirModal();
  }

  public cerrarModal(){
    this.categoriaService.cerrarModal();
  }

}
