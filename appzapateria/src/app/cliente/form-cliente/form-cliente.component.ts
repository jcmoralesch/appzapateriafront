import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../../model/service/cliente.service';
import { Cliente } from '../../model/class/cliente';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-form-cliente',
  templateUrl: './form-cliente.component.html',
  styleUrls: ['./form-cliente.component.css']
})
export class FormClienteComponent implements OnInit {

  public cliente: Cliente = new Cliente();

  constructor(public clienteService: ClienteService,
              private toastr: ToastrService) { }

  ngOnInit() {
  }

  store() {
     this.clienteService.store(this.cliente).subscribe(
       cliente => {
         this.clienteService.notificarCambio.emit(cliente);
         this.toastr.success('Cliente registrado con exito', 'Success');
         this.clienteService.cerrarModal();
       },
       error => {
         Swal.fire('Error', 'No fue posible regisgrar el cliente', 'error');
       }
     );
     }

  public cerrarModal() {
    this.clienteService.cerrarModal();
  }
}
