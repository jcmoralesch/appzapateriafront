import { Component, OnInit } from '@angular/core';
import { TipoProductoService } from '../model/service/tipo-producto.service';
import { ToastrService } from 'ngx-toastr';
import { TipoProducto } from '../model/class/tipo-producto';

@Component({
  selector: 'app-tipo-producto',
  templateUrl: './tipo-producto.component.html',
  styleUrls: ['./tipo-producto.component.css']
})
export class TipoProductoComponent implements OnInit {

  public tipoProductos:TipoProducto[];
  public tipoProducto:TipoProducto = new TipoProducto();

  constructor(public tipoProductoService:TipoProductoService,
              private toastr:ToastrService) { }

  ngOnInit() {

    this.getAll();
  }

  public store(){
    this.tipoProductoService.store(this.tipoProducto).subscribe(
      tipoProducto=>{
         this.getAll(),
         this.cerrarModal();
         this.toastr.success('Operación realizada con éxito','Success');
      },
      err=>{

      }
    )
  }

  private getAll():void{
    this.tipoProductoService.getAll().subscribe(
      tipoProducto=>this.tipoProductos=tipoProducto
    )
  }

  public abrirModal(){
    this.tipoProductoService.abrirModal();
  }

  public cerrarModal(){
    this.tipoProductoService.cerrarModal();
  }

}
