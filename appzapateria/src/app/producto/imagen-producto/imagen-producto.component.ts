import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/model/service/producto.service';
import { Producto } from 'src/app/model/class/producto';
import { TallaService } from 'src/app/model/service/talla.service';
import { Talla } from 'src/app/model/class/talla';
import { ProductoConsulta } from 'src/app/model/class/producto-consulta';
import { Marca } from 'src/app/model/class/marca';
import { MarcaService } from 'src/app/model/service/marca.service';
import { CategoriaService } from 'src/app/model/service/categoria.service';
import { Categoria } from 'src/app/model/class/categoria';
import { TipoProducto } from 'src/app/model/class/tipo-producto';
import { TipoProductoService } from 'src/app/model/service/tipo-producto.service';
import { ColorService } from 'src/app/model/service/color.service';
import { Color } from 'src/app/model/class/color';
import { LoginService } from 'src/app/model/service/login.service';
import { UsuarioService } from 'src/app/model/service/usuario.service';
import { CantidadProductoService } from 'src/app/model/service/cantidad-producto.service';
import { CantidadProducto } from 'src/app/model/class/cantidad-producto';
import { URL_BACKEND_IMAGE } from 'src/app/config/config';

@Component({
  selector: 'app-imagen-producto',
  templateUrl: './imagen-producto.component.html',
  styleUrls: ['./imagen-producto.component.css']
})
export class ImagenProductoComponent implements OnInit {

  productos:CantidadProducto[];
  marcas:Marca[];
  productoConsulta:ProductoConsulta= new ProductoConsulta();
  tallas:Talla[];
  categorias:Categoria[];
  tipoProductos:TipoProducto[];
  colors:Color[];
  agencia:string="";
  public urlImage:string=URL_BACKEND_IMAGE;

  constructor(private productoService:ProductoService,
              private tallaService:TallaService,
              private marcaService:MarcaService,
              private categoriaService:CategoriaService,
              private tipoProductoService:TipoProductoService,
              private colorService:ColorService,
              private loginService:LoginService,
              private usuarioService:UsuarioService,
              private cantidadProductoService:CantidadProductoService) { }

  ngOnInit() {
    this.findAgenciaNombre();
    this.getTallas();
    this.getMarcas();
    this.getCategorias();
    this.getTipoProducto();
    this.getColor();
  }

  findAgenciaNombre():void{
    this.usuarioService.findByUbicacion(this.loginService.id).subscribe(
      agencia=>{
        this.getAll(agencia.ubicacion);
        this.agencia=agencia.ubicacion;
      }
    )
  }

  getAll(agencia:string):void{
    this.cantidadProductoService.getByAgenciaAndCantidad(agencia).subscribe(
      productos=>this.productos=productos
    )
  }

  getTallas():void{
    this.tallaService.getAll().subscribe(
      tallas=>this.tallas=tallas
    );
  }

  getMarcas():void{
    this.marcaService.getAll().subscribe(
      marcas=>this.marcas=marcas
    )
  }

  getCategorias():void{
    this.categoriaService.findAll().subscribe(
      categorias=>this.categorias=categorias
    )
  }

  getTipoProducto():void{
    this.tipoProductoService.getAll().subscribe(
      tipoProductos=>this.tipoProductos=tipoProductos
    )
  }

  getColor():void{
    this.colorService.getAll().subscribe(
      colors=>this.colors=colors
    )
  }

  buscarProducto():void{
    this.cantidadProductoService.findProductosByFilter(this.productoConsulta.talla,this.productoConsulta.marca,this.agencia,
                                        this.productoConsulta.categoria,this.productoConsulta.tipoProducto,
                                        this.productoConsulta.color).subscribe(
      productos=>this.productos=productos
    );
  }
  

}
