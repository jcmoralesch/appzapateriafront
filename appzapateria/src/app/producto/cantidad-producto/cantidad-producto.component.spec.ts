import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CantidadProductoComponent } from './cantidad-producto.component';

describe('CantidadProductoComponent', () => {
  let component: CantidadProductoComponent;
  let fixture: ComponentFixture<CantidadProductoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CantidadProductoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CantidadProductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
