import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { CantidadProductoService } from 'src/app/model/service/cantidad-producto.service';
import { LoginService } from 'src/app/model/service/login.service';
import { UsuarioService } from 'src/app/model/service/usuario.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { CantidadProducto } from 'src/app/model/class/cantidad-producto';
import { ProductoService } from 'src/app/model/service/producto.service';
import { Producto } from 'src/app/model/class/producto';
@Component({
  selector: 'app-cantidad-producto',
  templateUrl: './cantidad-producto.component.html',
  styleUrls: ['./cantidad-producto.component.css']
})
export class CantidadProductoComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  
  public dataSource: any="";
  public producto: Producto;
  public nombreAgencia:string;
  public mostrarNavLateral:boolean=true;
  public texto: string =  "Ocultar";

  constructor(private cantidadProductoService: CantidadProductoService,
    private loginService: LoginService,
    private usuarioService: UsuarioService,
    private productoService: ProductoService) { }

  ngOnInit() {

    this.findUserByAgencia();
    
  }

  displayedColumns = ['Codigo', 'Precio', 'Categoria', 'Color', 'Marca', 'Talla', 'Tipo', 'Disponible', 'Acciones'];

  private findUserByAgencia(): void {
    this.usuarioService.findByUbicacion(this.loginService.id).subscribe(
      agencia => {
        this.getCantidadProductoByAgencia(agencia.ubicacion);
        this.nombreAgencia=agencia.ubicacion;
      }
    )
  }

  public getCantidadProductoByAgencia(agencia: string): void {
    this.cantidadProductoService.getAll(agencia).subscribe(
      response => {
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = response;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.dataSource.filterPredicate = this.tableFilter();

      }
    )
  }

  private tableFilter(): (data: CantidadProducto, filter: string) => boolean {
    let filterFunction = function (data, filter): boolean {
      return data.producto.codigo.indexOf(filter) != -1
        || data.producto.categoria.categoria.indexOf(filter) != -1
        || data.producto.tipoProducto.tipoProducto.indexOf(filter) != -1
        || data.producto.marca.marca.indexOf(filter) != -1
        || data.producto.color.color.indexOf(filter) != -1;
    }
    return filterFunction;
  }

  public applyFilter(filterValue: any) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toUpperCase();
    this.dataSource.filter = filterValue;
  }



  public abrirModal(row: any): void {
    this.producto = row.producto;
    this.productoService.abrirModal();
  }

  public findyByCantidad(value:any):void{
    this.cantidadProductoService.getByCantidad(this.nombreAgencia,value).subscribe(
      response => {
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = response;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.dataSource.filterPredicate = this.tableFilter();
      }
    )
  }

  cambiaVisibilidad() {
    this.texto = (this.mostrarNavLateral) ?  "Mostrar" : "Ocultar";
    this.mostrarNavLateral = !this.mostrarNavLateral; 
  }
}





