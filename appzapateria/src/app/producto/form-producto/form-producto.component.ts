import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/model/class/producto';
import { ProductoService } from 'src/app/model/service/producto.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { Categoria } from 'src/app/model/class/categoria';
import { CategoriaService } from 'src/app/model/service/categoria.service';
import { ColorService } from 'src/app/model/service/color.service';
import { MarcaService } from 'src/app/model/service/marca.service';
import { ProveedorService } from 'src/app/model/service/proveedor.service';
import { TallaService } from 'src/app/model/service/talla.service';
import { TipoProductoService } from 'src/app/model/service/tipo-producto.service';
import { Color } from 'src/app/model/class/color';
import { Marca } from 'src/app/model/class/marca';
import { Proveedor } from 'src/app/model/class/proveedor';
import { Talla } from 'src/app/model/class/talla';
import { TipoProducto } from 'src/app/model/class/tipo-producto';

@Component({
  selector: 'app-form-producto',
  templateUrl: './form-producto.component.html',
  styleUrls: ['./form-producto.component.css']
})
export class FormProductoComponent implements OnInit {

  public producto:Producto=new Producto();
  public categorias:Categoria[];
  public colors:Color[];
  public marcas:Marca[];
  public proveedors:Proveedor[];
  public tallas:Talla[];
  public tipoProductos:TipoProducto[];

  constructor(private productoService:ProductoService,
              private router:Router,
              private categoriaService:CategoriaService,
              private colorService:ColorService,
              private marcaService:MarcaService,
              private proveedorService:ProveedorService,
              private tallaService:TallaService,
              private tipoProducto:TipoProductoService,
              private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.getAllCategoria();
    this.getAllColor();
    this.getAllMarca();
    this.getAllProveedor();
    this.getAllTalla();
    this.getAllTipoProducto();
    this.cargarProducto();
  }

  public store():void{
    this.productoService.store(this.producto).subscribe(
      producto=>{
        this.router.navigate(['/producto']);
        Swal.fire('Registro nuevo','producto registrado correctamente','success');
      }
    )
  }

  public cargarProducto():void{
     this.activatedRoute.params.subscribe(
       param=>{
         let id=param['id'];
         if(id){
           this.productoService.getById(id).subscribe(
             producto=>this.producto=producto
           )
         }
       }
     )
  }

  public update():void{
    this.productoService.update(this.producto).subscribe(
      producto=>{
        this.router.navigate(['/producto']);
        Swal.fire('Actualizado','Datos de producto actualizado correctamente','success');
      }
    )
  }

  private getAllCategoria():void{
    this.categoriaService.findAll().subscribe(
      categoria=>this.categorias=categoria
    )
  }

  private getAllColor():void{
     this.colorService.getAll().subscribe(
       color=>this.colors=color
     )
  }

  private getAllMarca():void{
    this.marcaService.getAll().subscribe(
      marca=>this.marcas=marca
    )
  }

  private getAllProveedor():void{
    this.proveedorService.getAllPersonal().subscribe(
      proveedor=>this.proveedors=proveedor
    )
  }

  private getAllTalla():void{
    this.tallaService.getAll().subscribe(
      talla=>this.tallas=talla
    )
  }

  private getAllTipoProducto():void{
    this.tipoProducto.getAll().subscribe(
      tipoProducto=>this.tipoProductos=tipoProducto
    )
  }

  compararCategoria(o1:Categoria,o2:Categoria):boolean{
    if(o1===undefined && o2===undefined){
      return true;
    }
    return o1===null || o2===null || o1===undefined || o2===undefined? false: o1.id===o2.id;
  }

  compararColor(o1:Color,o2:Color):boolean{
    if(o1===undefined && o2===undefined){
      return true;
    }
    return o1===null || o2===null || o1===undefined || o2===undefined? false: o1.id===o2.id;
  }

  compararMarca(o1:Marca,o2:Marca):boolean{
    if(o1===undefined && o2===undefined){
      return true;
    }
    return o1===null || o2===null || o1===undefined || o2===undefined? false: o1.id===o2.id;
  }

  compararProveedor(o1:Proveedor,o2:Proveedor):boolean{
    if(o1===undefined && o2===undefined){
      return true;
    }
    return o1===null || o2===null || o1===undefined || o2===undefined? false: o1.id===o2.id;
  }

  compararTalla(o1:Talla,o2:Talla):boolean{
    if(o1===undefined && o2===undefined){
      return true;
    }
    return o1===null || o2===null || o1===undefined || o2===undefined? false: o1.id===o2.id;
  }

  compararTipoProducto(o1:TipoProducto,o2:TipoProducto):boolean{
    if(o1===undefined && o2===undefined){
      return true;
    }
    return o1===null || o2===null || o1===undefined || o2===undefined? false: o1.id===o2.id;
  }

}
