import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/model/class/usuario';
import { FormGroup, FormBuilder, FormArray, ValidatorFn, FormControl } from '@angular/forms';
import { Role } from 'src/app/model/class/role';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from 'src/app/model/service/usuario.service';
import { RoleService } from 'src/app/model/service/role.service';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';

@Component({
  selector: 'app-edit-usuario',
  templateUrl: './edit-usuario.component.html',
  styleUrls: ['./edit-usuario.component.css']
})
export class EditUsuarioComponent implements OnInit {

  usuario:Usuario = new Usuario();
  roleForm: FormGroup;
  roles:Role[]=[];

  constructor(private activatedRoute:ActivatedRoute,
    private usuarioService:UsuarioService,
    private fb:FormBuilder,
    private roleService:RoleService,
    private toastr:ToastrService,
    private router:Router) { 

      this.roleForm = this.fb.group({
        username:[''],
        password:[''],
        roles: new FormArray([], minSelectedCheckboxes(1)),

      });

      of(this.roleService.getAll().subscribe(
        roles=>{
          this.roles=roles;
          this.addCheckboxes();
        }
      ))
    }

  ngOnInit() {

    this.cargarUsuario();
  }

  private addCheckboxes() {
    this.roles.map((o, i) => {
      const control = new FormControl(i === 0); // if first item set to true, else false
      (this.roleForm.controls.roles as FormArray).push(control);
    });
  }

  public submit() {
    const selectedOrderIds = this.roleForm.value.roles
      .map((v, i) => v ? this.roles[i] : null)
      .filter(v => v !== null);
    this.usuario.role=selectedOrderIds;
 
    this.usuarioService.update(this.usuario).subscribe(
        usuario=>{
          this.router.navigate(['/list-usuario']);
          this.toastr.success("Roles de usuario Modificado con exito", "Success");
        }
     );
 
  }

  cargarUsuario():void{

    this.activatedRoute.params.subscribe(
      params=>{
        let id=params['id'];
        if(id){
          this.usuarioService.findUsuarioById(id).subscribe(
            usuario=>{
              this.usuario=usuario;
            }
          )
        }
      });
  }

}

function minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      .map(control => control.value)
      .reduce((prev, next) => next ? prev + next : prev, 0);

    return totalSelected >= min ? null : { required: true };
  };

  return validator;
}
