import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/model/class/usuario';
import { UsuarioService } from 'src/app/model/service/usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-usuario',
  templateUrl: './list-usuario.component.html',
  styleUrls: ['./list-usuario.component.css']
})
export class ListUsuarioComponent implements OnInit {

  public usuarios:Usuario[];
  public user:Usuario;

  constructor(public usuarioService:UsuarioService) { }

  ngOnInit() {
    this.getAll();
  }

  private getAll():void{
    this.usuarioService.getAll().subscribe(
      usuarios=>this.usuarios=usuarios
    )
  }

  public mostrarDetalle(usuario):void{
    this.user=usuario;
    this.usuarioService.abrirModal();
  }

  public delete(usuario:any):void{

    Swal.fire({
        title: 'Esta seguro?',
        text: `De eliminar al usuario ${usuario.username} !`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI, Eliminar !'
      }).then((result) => {

        if (result.value) {
         this.usuarioService.delete(usuario.id).subscribe(
           response=>{
             this.usuarios=this.usuarios.filter(usr=> usr !==usuario);
             Swal.fire(
               'Eliminado!',
               `El usuario ${usuario.username} ha sido eliminado`,
               'success'
             )
           }
         )

        }
      })

  }

  public cerrarModal():void{
    this.usuarioService.cerrarModal();
  }

}
