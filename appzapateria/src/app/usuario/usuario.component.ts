import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { Role } from '../model/class/role';
import { Usuario } from '../model/class/usuario';
import { Personal } from '../model/class/personal';
import { RoleService } from '../model/service/role.service';
import { PersonalService } from '../model/service/personal.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '../model/service/usuario.service';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  roleForm: FormGroup;
  roles: Role[] = [];
  usuario: Usuario = new Usuario();
  personal: Personal = new Personal();
  errores: string[];

  constructor(
    private fb: FormBuilder,
    private roleService: RoleService,
    private personalService: PersonalService,
    private activatedRoute: ActivatedRoute,
    private usuarioService: UsuarioService,
    private toastr: ToastrService,
    private router: Router) { }

  private cargarPersonal(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if (id) {
        this.personalService.getById(id).subscribe(
          personal => this.personal = personal
        )
      }
    });
  }

  ngOnInit() {
    this.cargarPersonal();
    this.roleForm = this.fb.group({
      username:[''],
      password:[''],
      roles: new FormArray([], minSelectedCheckboxes(1)),

    });

    of(this.roleService.getAll().subscribe(
      roles=>{
        this.roles=roles;
        this.addCheckboxes();
      }
    ))
  }

  private addCheckboxes() {
    this.roles.map((o, i) => {
      const control = new FormControl(i === 0); // if first item set to true, else false
      (this.roleForm.controls.roles as FormArray).push(control);
    });
  }

  submit() {
    const selectedOrderIds = this.roleForm.value.roles
      .map((v, i) => v ? this.roles[i] : null)
      .filter(v => v !== null);
 
    this.usuario.personal=this.personal;
    this.usuario.username=this.roleForm.value.username;
    this.usuario.password=this.roleForm.value.password;
    this.usuario.role=selectedOrderIds;
 
    this.usuarioService.store(this.usuario).subscribe(
        usuario=>{
          this.router.navigate(['/list-usuario']);
          this.toastr.success("Usuario creado con exito", "Success");
        },
        err=>{
          this.errores=err.error.errors as string[];
        }
     );
 
  }

}

function minSelectedCheckboxes(min = 1) {
  const validator: ValidatorFn = (formArray: FormArray) => {
    const totalSelected = formArray.controls
      .map(control => control.value)
      .reduce((prev, next) => next ? prev + next : prev, 0);

    return totalSelected >= min ? null : { required: true };
  };

  return validator;
}
