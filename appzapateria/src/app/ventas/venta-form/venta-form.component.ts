import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material';
import { ClienteService } from '../../model/service/cliente.service';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { Cliente } from 'src/app/model/class/cliente';
import { FormControl } from '@angular/forms';
import { CantidadProducto } from 'src/app/model/class/cantidad-producto';
import { Factura } from '../../model/class/factura';
import { ItemFactura } from 'src/app/model/class/item-factura';
import { Observable } from 'rxjs';
import { map, flatMap } from 'rxjs/operators';
import { FacturaService } from '../../model/service/factura.service';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from '../../model/service/login.service';
import { UsuarioService } from '../../model/service/usuario.service';

@Component({
  selector: 'app-venta-form',
  templateUrl: './venta-form.component.html',
  styleUrls: ['./venta-form.component.css']
})
export class VentaFormComponent implements OnInit {

  public clienteNew: Cliente = new Cliente();
  autocompleteControl = new FormControl();
  autocompleteControlCliente = new FormControl();
  public factura: Factura = new Factura();
  productosFiltrados: Observable<CantidadProducto[]>;
  clientesFiltrados: Observable<Cliente[]>;
  nombreAgencia:string;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(public clienteService: ClienteService,
              private facturaService: FacturaService,
              private toastr: ToastrService,
              public loginService:LoginService,
              private usuarioService:UsuarioService) { }

  ngOnInit() {
    this.clientesFiltrados = this.autocompleteControlCliente.valueChanges
      .pipe(
        map(value => typeof value === 'string' ? value : value.nombre), 
        flatMap(value => value ? this._filtercliente(value) : [])
      );
    this.cargarProductos();
    this.actualizarTablaDeseModal();
    this.mostrarAgencia();
  }

  mostrarAgencia():void{
    this.usuarioService.findByUbicacion(this.loginService.id).subscribe(
      agencia=>{
        this.nombreAgencia=agencia.ubicacion;
      }
    )
  }

  private cargarProductos():void{
    this.productosFiltrados = this.autocompleteControl.valueChanges
      .pipe(
        map(value => typeof value === 'string' ? value : value.producto.codigo), 
        flatMap(value => value ? this._filter(value) : [])
      );
  }

  private _filter(value: string): Observable<CantidadProducto[]> {
    const filterValue = value.toUpperCase();
    return this.facturaService.filtrarProductos(filterValue);
  }

  private _filtercliente(value: string) :Observable<Cliente[]>{
    const filterValue=value.toUpperCase();
    return this.clienteService.filtrarClientes(filterValue);
  }

  mostrarNombre(producto?: CantidadProducto): string | undefined {
    return producto ? producto.producto.codigo : undefined;
  }

  mostrardatosCliente(cliente?: Cliente): string | undefined {
    return cliente ? cliente.nombre : undefined;
  }

  abrirModalCliente() {
    this.clienteService.abrirModal();
  }

  seleccionarProducto(event: MatAutocompleteSelectedEvent): void {
    const producto = event.option.value as CantidadProducto;
    if (this.existeItem(producto.id)) {
      this.incrementaCantidad(producto.id);
    } else {
      const nuevoItem = new ItemFactura();
      nuevoItem.cantidadProducto = producto;
      this.factura.items.push(nuevoItem);
    }

    this.autocompleteControl.setValue('');
    event.option.focus();
    event.option.deselect();

  }


  seleccionarCliente(event: MatAutocompleteSelectedEvent):void{
    const cliente=event.option.value as Cliente;
    this.clienteNew =cliente;
    this.autocompleteControlCliente.setValue('');
    event.option.focus();
    event.option.deselect();
  }

  existeItem(id: number): boolean {
    let existe = false;
    this.factura.items.forEach((item: ItemFactura) => {
      if (id === item.cantidadProducto.id) {
        existe = true;
      }
    });
    return existe;
  }

  incrementaCantidad(id: number): void {
    this.factura.items = this.factura.items.map((item: ItemFactura) => {
      if (id === item.cantidadProducto.id) {
        ++item.cantidad;
      }
      return item;
    });
  }

  eliminarItemFactura(id: number): void {
    this.factura.items = this.factura.items.filter((item: ItemFactura) => id !== item.cantidadProducto.producto.id);
  }


  actualizarPrecio(id: number, event: any,pBase:number): void {
    let precio: number = event.target.value as number;

    if (precio<pBase) {
      Swal.fire('Error', 'Esta ingresando un precio menor a lo establecido, Si no lo corrige no podra registrar la venta', 'error');
      return ;
    }

    this.factura.items = this.factura.items.map((item: ItemFactura) => {
      if (id === item.cantidadProducto.producto.id) {
        item.cantidadProducto.producto.precioVenta= precio;
        item.precioVenta=item.cantidadProducto.producto.precioVenta;
      }
      return item;
    });
  }

  actualizarCantidad(id: number, event: any,cantBase:number): void {
    let cantidad: number = event.target.value as number;

    if (cantidad>cantBase) {
      Swal.fire("Error",`Ya no hay productos en EXISTENCIA, solo se puede vender ${cantBase}`,'error');
      return ;
    }

    if (cantidad == 0) {
      return this.eliminarItemFactura(id);
    }

    this.factura.items = this.factura.items.map((item: ItemFactura) => {
      if (id === item.cantidadProducto.producto.id) {
        item.cantidad = cantidad;
      }
      return item;
    });
  }

  create(facturaForm): void {

    if (this.factura.items.length == 0) {
      this.autocompleteControl.setErrors({ 'invalid' : true });
    }  
    if (facturaForm.form.valid && this.factura.items.length > 0) {

      if(this.clienteNew.nombre=="" && this.clienteNew.id==0){
        this.factura.cliente = null;
      }
      else{
          this.factura.cliente = this.clienteNew;
      }
    
      this.facturaService.create(this.factura).subscribe(factura => {
        this.facturaService.notificarCambio.emit();//para actualizar datos en la tabla
        this.toastr.success('Venta registrada con exito', 'Exito');
        let i=0;
        let cantidad=this.factura.items.length;
        for(i;i<=cantidad;i++){
          this.factura.items.pop();
        }
        this.clienteNew.nombre="";
        this.clienteNew.id=0;
        this.cargarProductos();
      },
      err=>{
          Swal.fire('Error','No se pudo registrar la venta','error');
      });
    }   
  }

  private actualizarTablaDeseModal():void{
    this.clienteService.notificarCambio.subscribe(
      cliente=>this.clienteNew=cliente
    )
  }

}