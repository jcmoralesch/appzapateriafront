import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { ConsultaFecha } from 'src/app/model/class/consulta-fecha';
import { Agencia } from 'src/app/model/class/agencia';
import { FacturaService } from '../../model/service/factura.service';
import { Factura } from '../../model/class/factura';
import { AgenciaService } from '../../model/service/agencia.service';

@Component({
  selector: 'app-historial-venta',
  templateUrl: './historial-venta.component.html',
  styleUrls: ['./historial-venta.component.css']
})
export class HistorialVentaComponent implements OnInit {

  public dataSource:any;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public consultaFecha:ConsultaFecha = new ConsultaFecha();
  public agencias:Agencia[];
  public valor:boolean=true;
  public total:number;
  public ganancia:number;
  public fac:Factura;

  constructor(private facturaService:FacturaService,
              private agenciaService:AgenciaService) { }

  ngOnInit() {
    this.getAllAgenciaAndDate();
    this.getAllAgencias();
  }

  displayedColumns = ['Fecha','Hora','Cliente','Ganancia','Total','Registrado','Acciones'];

  private getAllAgenciaAndDate():void{
    this.facturaService.getAll().subscribe(
      response=>{
        this.total=response.map(t=> t.total).reduce((acc,value)=>acc+value,0);
        this.ganancia=response.map(t=>t.gananciaTotal).reduce((acc,value)=>acc+value,0);
        this.valor=false;
        this.dataSource = new MatTableDataSource();
        this.dataSource.data=response;
        this.dataSource.sort=this.sort;
        this.dataSource.paginator=this.paginator;
      }
    )
  }


  private getAllAgencias():void{
      this.agenciaService.getAll().subscribe(
        agencias=>this.agencias=agencias
      )
  }



  public getByFechaAndAgencia():void{
    this.facturaService.getByFechaAndAgencia(this.consultaFecha.fecha1.toISOString(),
                                          this.consultaFecha.fecha2.toISOString(),this.consultaFecha.agencia).subscribe(
     response=>{
       this.total=response.map(t=> t.total).reduce((acc,value)=>acc+value,0);
       this.ganancia=response.map(t=> t.gananciaTotal).reduce((acc,value)=>acc+value,0);
       this.valor=false;
       this.dataSource = new MatTableDataSource();
       this.dataSource.data=response;
       this.dataSource.sort=this.sort;
       this.dataSource.paginator=this.paginator;
     }
    )
 }

 public modalDetalle(row:any):void{
  this.fac=row;
  this.facturaService.abrirModal(); 
}


}
