import { HttpClient, HttpHeaders } from '@angular/common/http';
export const URL_BACKEND='http://35.175.135.94:8080/api';
export const URL_BACKEND_IMAGE='https://s3.amazonaws.com/imageszapateria/';
//export const URL_BACKEND='http://localhost:8081/api';
export const HTTPHEADERS:HttpHeaders = new HttpHeaders({'Content-Type':'application/json'});