import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgenciaComponent } from './agencia/agencia.component';
import { FormAgenciaComponent } from './agencia/form-agencia/form-agencia.component';
import { PersonalComponent } from './personal/personal.component';
import { FormPersonalComponent } from './personal/form-personal/form-personal.component';
import { ProveedoresComponent } from './proveedores/proveedores.component';
import { ColoresComponent } from './colores/colores.component';
import { FormProveedorComponent } from './proveedores/form-proveedor/form-proveedor.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { TallaComponent } from './talla/talla.component';
import { TipoProductoComponent } from './tipo-producto/tipo-producto.component';
import { MarcaComponent } from './marca/marca.component';
import { ProductoComponent } from './producto/producto.component';
import { FormProductoComponent } from './producto/form-producto/form-producto.component';
import { DetalleComponent } from './producto/detalle/detalle.component';
import { ImagenProductoComponent } from './producto/imagen-producto/imagen-producto.component';
import { LoginComponent } from './usuario/login/login.component';
import { IngresoComponent } from './ingreso-producto/ingreso/ingreso.component';
import { ListTrasladoComponent } from './traslado/list-traslado/list-traslado.component';
import { CantidadProductoComponent } from './producto/cantidad-producto/cantidad-producto.component';
import { TodoCantidadProductoComponent } from './producto/todo-cantidad-producto/todo-cantidad-producto.component';
import { ListUsuarioComponent } from './usuario/list-usuario/list-usuario.component';
import { EditUsuarioComponent } from './usuario/edit-usuario/edit-usuario.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { ModificarUsuarioComponent } from './usuario/modificar-usuario/modificar-usuario.component';
import { VentaFormComponent } from './ventas/venta-form/venta-form.component';
import { HistorialVentaComponent } from './ventas/historial-venta/historial-venta.component';
import { ListTrasladoEntreAgenciaComponent } from './trasladoEntreAgencia/list-traslado-entre-agencia/list-traslado-entre-agencia.component';

const routes: Routes = [
  {path:'',redirectTo:'/login',pathMatch:'full'},
  {path: 'login',component:LoginComponent},
  {path:'agencia',component:AgenciaComponent},
  {path:'agencia/form',component:FormAgenciaComponent},
  {path:'personal',component:PersonalComponent},
  {path:'personal/page/:page',component:PersonalComponent},
  {path:'personal/form',component:FormPersonalComponent},
  {path:'personal/form/:id',component:FormPersonalComponent},
  {path:'proveedor',component:ProveedoresComponent},
  {path:'proveedor/form',component:FormProveedorComponent},
  {path:'proveedor/page/:page',component:ProveedoresComponent},
  {path:'proveedor/form/:id',component:FormProveedorComponent},
  {path:'color',component:ColoresComponent},
  {path:'categoria',component:CategoriaComponent},
  {path:'talla',component:TallaComponent},
  {path:'tipo-producto',component:TipoProductoComponent},
  {path:'marca',component:MarcaComponent},
  {path:'producto',component:ProductoComponent},
  {path:'producto/form',component:FormProductoComponent},
  {path:'producto/form/:id',component:FormProductoComponent},
  {path:'producto/detalle/:id',component:DetalleComponent},
  {path:'producto/images',component:ImagenProductoComponent},
  {path:'ingreso',component:IngresoComponent},
  {path:'traslado',component:ListTrasladoComponent},
  {path:'cantidad',component:CantidadProductoComponent},
  {path:'todo-cantidad',component:TodoCantidadProductoComponent},
  {path: 'venta', component:VentaFormComponent},
  {path:'historial-venta', component:HistorialVentaComponent},
  {path:'list-usuario',component:ListUsuarioComponent},
  {path:'usuario/edit/:id',component:EditUsuarioComponent},
  {path:'usuario/form/:id',component:UsuarioComponent},
  {path:'usuario/update-user/:username',component:ModificarUsuarioComponent},
  {path:'traslado-entre-agencias',component:ListTrasladoEntreAgenciaComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
