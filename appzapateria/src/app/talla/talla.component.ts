import { Component, OnInit } from '@angular/core';
import { Talla } from '../model/class/talla';
import { TallaService } from '../model/service/talla.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-talla',
  templateUrl: './talla.component.html',
  styleUrls: ['./talla.component.css']
})
export class TallaComponent implements OnInit {

  public tallas:Talla[];
  public talla:Talla = new Talla();

  constructor(public tallaService:TallaService,
              private toastr:ToastrService) { }

  ngOnInit() {
    this.getAll();
  }

  public store():void{
    this.tallaService.store(this.talla).subscribe(
      talla=>{
        this.getAll();
        this.cerrarModal();
        this.toastr.success('Operación realizada con éxito','Success');
      },
      err=>{
        
      }
    )
  }

  private getAll():void{
    this.tallaService.getAll
    this.tallaService.getAll().subscribe(
      talla=>this.tallas=talla
    )
  }

  public abrirModal(){
    this.tallaService.abrirModal();
  }

  public cerrarModal(){
    this.tallaService.cerrarModal();
  }

}
