import { CantidadProducto } from './cantidad-producto';

export class ItemFactura {

    cantidadProducto:CantidadProducto;
    cantidad:number=1;
    importe:number;
    precioVenta:number=0;
    ganancia:number;
    descuento:number;

    public calcularImporte():number{
        return this.cantidad * this.cantidadProducto.producto.precioVenta;
    }
}
