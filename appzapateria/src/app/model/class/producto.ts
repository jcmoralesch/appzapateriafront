import { Categoria } from './categoria';
import { Color } from './color';
import { Marca } from './marca';
import { Proveedor } from './proveedor';
import { Talla } from './talla';
import { TipoProducto } from './tipo-producto';

export class Producto {
    public id:number;
    public precio:number=0.00;
    public categoria:Categoria=null;
    public codigo:string='';
    public precioVenta:number=0.00;
    public color:Color=null;
    public marca:Marca=null;
    public proveedor:Proveedor=null;
    public talla:Talla=null;
    public tipoProducto:TipoProducto=null;
    public alerta:number;
    public foto:string;
    
}
