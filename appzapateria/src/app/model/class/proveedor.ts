import { Empresa } from './empresa';

export class Proveedor {
    public id:number;
    public identificacion:string='';
    public nombre:string='';
    public apellido:string='';
    public telefono:string='';
    public email:string='';
    public empresa:Empresa;

}
