import { ItemFactura } from './item-factura';
import { Cliente } from './cliente';
import { Usuario } from './usuario';

export class Factura {
    id:number;
    items:Array<ItemFactura> =[];
    total:number;
    fecha:string;
    cliente:Cliente;
    hora:string;
    usuario:Usuario;
    gananciaTotal:number=0.00;

    calcularGranTotal():number{
        this.total=0;
        this.items.forEach((item: ItemFactura)=>{
            this.total += item.calcularImporte();
        });
        return this.total;
    }
}
