export class ProductoConsulta {
    public talla:string;
    public marca:string;
    public categoria:string;
    public tipoProducto:string;
    public color:string;
}
