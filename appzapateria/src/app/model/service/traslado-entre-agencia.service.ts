import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_BACKEND } from '../../config/config';
import { TrasladoEntreAgencia } from '../class/traslado-entre-agencia';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class TrasladoEntreAgenciaService {

  public modal:boolean=false;
  private urlEndPoint:string=URL_BACKEND+'/traslado-entre-agencia';
  private _notificarCambio= new EventEmitter<any>();

  constructor(private http:HttpClient) { }

  public get notificarCambio(){
    return this._notificarCambio;
  }

  public store(trasladoEntreAgencia:TrasladoEntreAgencia):Observable<TrasladoEntreAgencia>{
    return this.http.post<TrasladoEntreAgencia>(`${this.urlEndPoint}`,trasladoEntreAgencia).pipe(
      map((response:any)=>{
        console.log(response)
        console.log(response.trasladoProductoEntreAgencia)
        console.log(response.trasladoProducto)
        return response.trasladoProductoEntreAgencia as TrasladoEntreAgencia})
      ,
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public findByFechaBetween(fecha1:string,fecha2:string):Observable<TrasladoEntreAgencia[]>{
      return this.http.get<TrasladoEntreAgencia[]>(`${this.urlEndPoint}/${fecha1}/${fecha2}`);
  }

  public abrirModal():void{
    this.modal=true;
  }

  public cerrarModal():void{
    this.modal=false;
  }


}
