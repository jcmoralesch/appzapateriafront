import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_BACKEND } from '../../config/config';
import { Cliente } from '../class/cliente';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  private urlEndPoint:string=URL_BACKEND+'/cliente';
  public modal:boolean=false;
  private _notificarCambio= new EventEmitter<any>();

  get notificarCambio():EventEmitter<any>{
      return this._notificarCambio;
  }

  constructor(private http:HttpClient) { }

  public store(cliente:Cliente):Observable<Cliente>{
    return this.http.post(`${this.urlEndPoint}`,cliente).pipe(
      map((response:any)=>response.cliente as Cliente),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
      
    )
  }

  public filtrarClientes(term:string):Observable<Cliente[]>{
    return this.http.get<Cliente[]>(`${this.urlEndPoint}/filtrar-clientes/${term}`);
  }

  public cerrarModal():void{
    this.modal=false;
  }

  public abrirModal():void{
    this.modal=true;
  }
}
