import { Injectable } from '@angular/core';
import { URL_BACKEND } from 'src/app/config/config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Role } from '../class/role';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  private urlEndPoint:string=URL_BACKEND+'/role';

  constructor(private http:HttpClient) { }

  public getAll():Observable<Role[]>{
    return this.http.get<Role[]>(`${this.urlEndPoint}`);
  }
}

