import { Injectable } from '@angular/core';
import {URL_BACKEND,HTTPHEADERS} from '../../config/config';

import { IngresoProducto } from '../class/ingreso-producto';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {map, catchError} from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class IngresoProductoService {

  public modal:boolean=false;
  private urlEndPoint:string=URL_BACKEND+'/ingreso-producto';

  constructor(private http:HttpClient) { }

  public store(ingresoProducto:IngresoProducto):Observable<IngresoProducto>{
    return this.http.post<IngresoProducto>(`${this.urlEndPoint}`,ingresoProducto).pipe(
      map((response:any)=>response.ingresoProducto as IngresoProducto),
      catchError(e=>{
        if(e.status==400){
          return throwError(e)
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public getAll():Observable<IngresoProducto[]>{
    return this.http.get<IngresoProducto[]>( `${this.urlEndPoint}`);
  }

  public findByFechaIngresoBetween(fecha1,fecha2):Observable<IngresoProducto[]>{
     return this.http.get<IngresoProducto[]>(`${this.urlEndPoint}/${fecha1}/${fecha2}`);
  }

  public abrirModal():void{
    this.modal=true;
  }

  public cerrarModal():void{
    this.modal=false;
  }

  
}
