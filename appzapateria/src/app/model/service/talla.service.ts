import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_BACKEND, HTTPHEADERS } from 'src/app/config/config';
import { Talla } from '../class/talla';
import { Observable, throwError } from 'rxjs';
import { catchError,map } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class TallaService {

  private urlEndPoint:string=URL_BACKEND+'/talla';
  public modal:boolean=false;

  constructor(private http:HttpClient) { }

  public store(talla:Talla):Observable<Talla>{
    return this.http.post<Talla>(`${this.urlEndPoint}`,talla,{headers:HTTPHEADERS}).pipe(
      map((response:any)=> response.talla as Talla),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public getAll():Observable<Talla[]>{
    return this.http.get<Talla[]>(`${this.urlEndPoint}`);
  }

  public abrirModal(){
    this.modal=true;
  }

  public cerrarModal(){
    this.modal=false;
  }

}
