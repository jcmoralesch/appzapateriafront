import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { CantidadProducto } from '../class/cantidad-producto';
import { HttpClient } from '@angular/common/http';
import { URL_BACKEND } from '../../config/config';
import { Factura } from '../class/factura';

@Injectable({
  providedIn: 'root'
})
export class FacturaService {

  private urlEndPoint:string =URL_BACKEND+'/factura';
  private _notificarCambio= new EventEmitter<any>();
  public modal:boolean=false;
  
  constructor(private http: HttpClient) { }

  filtrarProductos(term: string): Observable<CantidadProducto[]> {
    return this.http.get<CantidadProducto[]>(`${this.urlEndPoint}/filtrar-productos/${term}`);
  }

  create(factura: Factura): Observable<Factura> {
    return this.http.post<Factura>(this.urlEndPoint, factura);
  }

  get notificarCambio(): EventEmitter<any>{
    return this._notificarCambio;
  }

  public getAll():Observable<Factura[]>{
    return this.http.get<Factura[]>(`${this.urlEndPoint}`);
  }

  public getByFechaAndAgencia(fecha1:string,fecha2:string,agencia:string):Observable<Factura[]>{
    return this.http.get<Factura[]>(`${this.urlEndPoint}/${fecha1}/${fecha2}/${agencia}`);
  }

  public abrirModal():void{
    this.modal=true;
  }

  public cerrarModal():void{
    this.modal=false;
  }

}
