import { Injectable } from '@angular/core';
import {URL_BACKEND} from '../../config/config'
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import { Usuario } from '../class/usuario';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private urlEndPoint:string=URL_BACKEND+'/usuario';
  public modal:boolean=false;


  constructor(private http:HttpClient) { }

  public findByUbicacion(id:number):Observable<any>{
    return this.http.get<any>(`${this.urlEndPoint}/ubicacion/${id}`).pipe(
      map((response:any)=>{
        response.content as String;
        return response
      })
    )
  }

  public getAll():Observable<Usuario[]>{
    return this.http.get<Usuario[]>(`${this.urlEndPoint}`);
  }

  public delete(id:number):Observable<Usuario>{
    return this.http.delete<Usuario>(`${this.urlEndPoint}/${id}`).pipe(
      catchError(e=>{
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public update(usuario:Usuario):Observable<Usuario>{
    return this.http.put<Usuario>(`${this.urlEndPoint}/${usuario.id}`,usuario);
  }

  public findUsuarioById(id:number):Observable<Usuario>{
    return this.http.get<Usuario>(`${this.urlEndPoint}/${id}`);
  }

  public store(usuario:Usuario):Observable<Usuario>{
    return this.http.post<Usuario>(`${this.urlEndPoint}`,usuario).pipe(
      map((response:any)=>response.usuario as Usuario),
      catchError(e=>{
        if(e.status==400){
          return throwError(e)
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public findByUsername(username:string):Observable<Usuario>{
    return this.http.get<Usuario>(`${this.urlEndPoint}/actualizar/${username}`);
  }

  public updatePassUser(usuario:Usuario):Observable<Usuario>{
    return this.http.put<Usuario>(`${this.urlEndPoint}/update/${usuario.id}`,usuario);
  }

  public abrirModal():void{
    this.modal=true;
  }

  public cerrarModal():void{
    this.modal=false;
  }
}
