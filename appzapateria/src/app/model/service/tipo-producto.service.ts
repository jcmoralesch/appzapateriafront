import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_BACKEND, HTTPHEADERS } from 'src/app/config/config';
import { TipoProducto } from '../class/tipo-producto';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class TipoProductoService {

  private urlEndPoint:string=URL_BACKEND+'/tipo-producto';
  public modal:boolean=false;

  constructor(private http:HttpClient) { }

  public store(tipoProducto:TipoProducto):Observable<TipoProducto>{
    return this.http.post<TipoProducto>(`${this.urlEndPoint}`,tipoProducto,{headers:HTTPHEADERS}).pipe(
      map((response:any)=>response.tipoProducto as TipoProducto),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public getAll():Observable<TipoProducto[]>{
    return this.http.get<TipoProducto[]>(`${this.urlEndPoint}`);
  }

  public abrirModal(){
    this.modal=true;
  }

  public cerrarModal(){
    this.modal=false;
  }
}
