import { Injectable } from '@angular/core';
import { URL_BACKEND } from 'src/app/config/config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CantidadProducto } from '../class/cantidad-producto';


@Injectable({
  providedIn: 'root'
})
export class CantidadProductoService {

  private urlEndPoint:string=URL_BACKEND+'/cantidad-producto';

  constructor(private http:HttpClient) { }

  public getAll(agencia:string):Observable<CantidadProducto[]>{
    return this.http.get<CantidadProducto[]>(`${this.urlEndPoint}/${agencia}`)
  }

  public getByAgenciaAndCantidad(agencia:string):Observable<CantidadProducto[]>{
    return this.http.get<CantidadProducto[]>(`${this.urlEndPoint}/agencia/cant/${agencia}`);
  }

  public getByCantidad(agencia:string,cantidad:number):Observable<CantidadProducto[]>{
    return this.http.get<CantidadProducto[]>(`${this.urlEndPoint}/${agencia}/${cantidad}`);
  }

  public getByCantidadAll(cantidad:number):Observable<CantidadProducto[]>{
    return this.http.get<CantidadProducto[]>(`${this.urlEndPoint}/cantidad/${cantidad}`);
  }

  public getAllAgencias():Observable<CantidadProducto[]>{
    return this.http.get<CantidadProducto[]>(`${this.urlEndPoint}`);
  }

  public findProductosByFilter(talla:string,marca:string,agencia:string,categoria:string,tipoProducto:string,color:string):Observable<CantidadProducto[]>{
    return this.http.get<CantidadProducto[]>(`${this.urlEndPoint}/filtrar/${talla}/${marca}/${agencia}/${categoria}/${tipoProducto}/${color}`);
  }
}
