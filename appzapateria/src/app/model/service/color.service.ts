import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_BACKEND, HTTPHEADERS } from 'src/app/config/config';
import { Color } from '../class/color';
import { Observable, throwError } from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ColorService {
  private urlEndPoint:string=URL_BACKEND+'/color';
  public modal:boolean=false;

  constructor(private http:HttpClient) { }

  public store(color:Color):Observable<Color>{
    return this.http.post<Color>(`${this.urlEndPoint}`,color,{headers:HTTPHEADERS}).pipe(
      map((response:any)=>response.color as Color),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public getAll():Observable<Color[]>{
    return this.http.get<Color[]>(`${this.urlEndPoint}`);
  }

  public abrirModal():void{
    this.modal=true;
  }

  public cerrarModal():void{
    this.modal=false;
  }
}
