import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTrasladoComponent } from './list-traslado.component';

describe('ListTrasladoComponent', () => {
  let component: ListTrasladoComponent;
  let fixture: ComponentFixture<ListTrasladoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTrasladoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTrasladoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
