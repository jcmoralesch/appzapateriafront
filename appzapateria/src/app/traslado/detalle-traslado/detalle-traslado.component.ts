import { Component, OnInit, Input } from '@angular/core';
import { TrasladoProductoService } from 'src/app/model/service/traslado-producto.service';
import { TrasladoProducto } from 'src/app/model/class/traslado-producto';
import { URL_BACKEND_IMAGE } from 'src/app/config/config';

@Component({
  selector: 'app-detalle-traslado',
  templateUrl: './detalle-traslado.component.html',
  styleUrls: ['./detalle-traslado.component.css']
})
export class DetalleTrasladoComponent implements OnInit {

  @Input() trasladoProducto:TrasladoProducto;
  public urlImage:string=URL_BACKEND_IMAGE;

  constructor(public trasladoProductoService:TrasladoProductoService) { }

  ngOnInit() {
  }

  public cerrarModal():void{
    this.trasladoProductoService.cerrarModal();
  }

}
