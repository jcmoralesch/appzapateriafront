import { Component, OnInit, Input } from '@angular/core';
import { TrasladoProductoService } from '../model/service/traslado-producto.service';
import { TrasladoProducto } from '../model/class/traslado-producto';
import { Agencia } from '../model/class/agencia';
import { AgenciaService } from '../model/service/agencia.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { IngresoProducto } from '../model/class/ingreso-producto';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-traslado',
  templateUrl: './traslado.component.html',
  styleUrls: ['./traslado.component.css']
})
export class TrasladoComponent implements OnInit {

  @Input() ingresoProducto:IngresoProducto;
  public trasladoProducto:TrasladoProducto = new TrasladoProducto();
  public agencias:Agencia[];
  

  constructor(public trasladoProductoService:TrasladoProductoService,
              private agenciaService:AgenciaService,
              private router:Router,
              private toastr:ToastrService) { }

  ngOnInit() {
    this.getAllAgencia();
  }

  public store():void{
   
    if(this.trasladoProducto.cantidad>this.ingresoProducto.traslado){
      Swal.fire('Error', `Esta trasladando una cantidad mayor a lo permitido. Solo puede trasladar ${this.ingresoProducto.traslado}`,'warning')
      return;
    }
    
    this.trasladoProducto.ingresoProducto=this.ingresoProducto;
    this.trasladoProductoService.store(this.trasladoProducto).subscribe(
      trasladoProducto=>{ 
        this.ingresoProducto.traslado=this.ingresoProducto.traslado-trasladoProducto.cantidad
        this.trasladoProductoService.notificarCambio.emit(this.ingresoProducto);//para actualizar datos en la tabla
        this.router.navigate(['/ingreso']);
        this.cerrarModal();
        this.toastr.success('Operación realizada con éxito','Success');
      }
    )
  }

  private getAllAgencia():void{
    this.agenciaService.getAll().subscribe(
      agencias=>this.agencias=agencias
    )
  }

  cerrarModal():void{
    this.trasladoProductoService.cerrarModal();
  }
}
