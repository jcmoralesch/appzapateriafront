import { Component, OnInit, Input } from '@angular/core';
import { Factura } from 'src/app/model/class/factura';
import { FacturaService } from '../../model/service/factura.service';
import { LoginService } from '../../model/service/login.service';

@Component({
  selector: 'app-detalle-factura',
  templateUrl: './detalle-factura.component.html',
  styleUrls: ['./detalle-factura.component.css']
})
export class DetalleFacturaComponent implements OnInit {

  @Input() factura:Factura;

  constructor(public  facturaService: FacturaService,
              public loginService:LoginService ) { }

  ngOnInit() {
  }

  public cerrarModal():void{
    this.facturaService.cerrarModal();
  }

}
