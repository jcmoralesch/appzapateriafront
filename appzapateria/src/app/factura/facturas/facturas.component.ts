import { Component, OnInit, ViewChild } from '@angular/core';
import { Factura } from 'src/app/model/class/factura';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { FacturaService } from '../../model/service/factura.service';

@Component({
  selector: 'app-facturas',
  templateUrl: './facturas.component.html',
  styleUrls: ['./facturas.component.css']
})
export class FacturasComponent implements OnInit {

  public facturas:Factura[];
  public factura:Factura;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public dataSource:any;
  public total:number;

  constructor(private facturaService:FacturaService) { }

  ngOnInit() {
    this.getAll();
    this.actualizarTabla();
  }

  displayedColumns = ['Fecha','Hora','Cliente','Total','Acciones'];

  private getAll():void{
    this.facturaService.getAll().subscribe(
      facturas=>{
        this.total=facturas.map(t=> t.total).reduce((acc,value)=>acc+value,0);
        this.dataSource = new MatTableDataSource();
        this.dataSource.data=facturas;
        this.dataSource.sort=this.sort;
        this.dataSource.paginator=this.paginator;
      }
    )
  }

  public abrirModal(row:Factura):void{
     this.factura=row;
     this.facturaService.abrirModal();

  }

  private actualizarTabla():void{
    this.facturaService.notificarCambio.subscribe(
      factura=>this.getAll()
    )
  }

}
