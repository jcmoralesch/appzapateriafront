import { Component, OnInit, Input } from '@angular/core';
import { CantidadProducto } from '../../model/class/cantidad-producto';
import { TrasladoEntreAgenciaService } from '../../model/service/traslado-entre-agencia.service';
import { TrasladoEntreAgencia } from '../../model/class/traslado-entre-agencia';
import { AgenciaService } from '../../model/service/agencia.service';
import { Agencia } from '../../model/class/agencia';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-traslado',
  templateUrl: './form-traslado.component.html',
  styleUrls: ['./form-traslado.component.css']
})
export class FormTrasladoComponent implements OnInit {

  @Input() cantidadProducto:CantidadProducto;
  public trasladoEntreAgencia:TrasladoEntreAgencia = new TrasladoEntreAgencia();
  public agencias:Agencia[];

  constructor(public trasladoEntreAgenciaService:TrasladoEntreAgenciaService,
              public agenciaService:AgenciaService) { }

  ngOnInit() {

    this.getAllAgencias();
  }

  public cerrarModal():void{
    this.trasladoEntreAgenciaService.cerrarModal();
  }

  private getAllAgencias():void{
    this.agenciaService.getAll().subscribe(
      agencias=>this.agencias=agencias,
      err=>console.log('Error al realizar la carga de agencias')
    )
  }

  public storeTraslado(){
    this.trasladoEntreAgencia.agenciaOrigen=this.cantidadProducto.agencia;
    this.trasladoEntreAgencia.cantidadProducto=this.cantidadProducto;

    this.trasladoEntreAgenciaService.store(this.trasladoEntreAgencia).subscribe(
      traslado=>{
          Swal.fire('Exito',`Se traslado correctamente el producto ${traslado.cantidadProducto.producto.codigo} 
                     a la agencia ${traslado.agenciaDestino.nombre}`,'success');
          this.cerrarModal();
          this.trasladoEntreAgenciaService.notificarCambio.emit();
      },
      error=>{
        Swal.fire('ERROR', `No se pudo hacer el traslado, El problema se podria deber porque el 
                   producto que esta intentando trasladar, no esta registrado en la agencia ${this.trasladoEntreAgencia.agenciaDestino.nombre}`,'error');
      }
    )
  }

}
