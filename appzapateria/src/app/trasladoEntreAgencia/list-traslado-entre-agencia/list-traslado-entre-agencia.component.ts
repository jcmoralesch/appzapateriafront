import { Component, OnInit, ViewChild } from '@angular/core';
import { TrasladoEntreAgenciaService } from '../../model/service/traslado-entre-agencia.service';
import { ConsultaFecha } from 'src/app/model/class/consulta-fecha';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

@Component({
  selector: 'app-list-traslado-entre-agencia',
  templateUrl: './list-traslado-entre-agencia.component.html',
  styleUrls: ['./list-traslado-entre-agencia.component.css']
})
export class ListTrasladoEntreAgenciaComponent implements OnInit {

  public dataSource:any;
  public consultaFecha:ConsultaFecha = new ConsultaFecha();
  public valor:boolean=true;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private trasladoAgencias:TrasladoEntreAgenciaService) { }

  ngOnInit() {
  }

  
  displayedColumns = ['Codigo','Origen','Destino','Cantidad','Fecha','Hora','Trasladado'];

  public getByDate():void{
    this.trasladoAgencias.findByFechaBetween(this.consultaFecha.fecha1.toISOString(),
                                 this.consultaFecha.fecha2.toISOString()).subscribe(
     response=>{
       console.log(response)
       this.valor=false;
       this.dataSource = new MatTableDataSource();
       this.dataSource.data=response;
       this.dataSource.sort=this.sort;
       this.dataSource.paginator=this.paginator;
     }
    )
 }

}
